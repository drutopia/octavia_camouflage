(document.querySelectorAll('.js-modal-trigger') || []).forEach(($trigger) => {
    const modal = $trigger.dataset.target;
    const $target = document.getElementById(modal);

    $trigger.addEventListener('click', () => {
      removeCategoriesLinks();
      selectIssues();
    });
});

function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
}

function removeCategoriesLinks()  {
    let categoriesAnchors = document.querySelectorAll(".category-list h2");
    for(let i=0; i<categoriesAnchors.length; i++) {
        categoriesAnchors[i].outerHTML = categoriesAnchors[i].outerHTML.replace(/<\/?a[^>]*>/g, "");
    };
}

function syncSameIssues(target) {

    let splitRegex = new RegExp(`(?<=[\)\"]), `);
    let input = document.getElementById("edit-field-topics-target-id");
    let value = target.innerHTML;
    let regexValue = escapeRegExp(value);
    let issueID = target.getAttribute('issue-id');
    let taxonomyRegex = new RegExp(`(")?${regexValue} \\(${issueID}\\)(")?`);
    let selectedIssuesList = document.querySelector("#selected-issues-list");
    let sameIssues = document.querySelectorAll(`[issue-id="${issueID}"]`)
    
    if (selectedIssuesList.contains(target)) {
        target.closest("li").remove();
        input.value = input.value.split(splitRegex).filter(substring => !taxonomyRegex.test(substring)).join(', ');
    }

    else if (target.classList.contains("selected")) {
        input.value = input.value.split(splitRegex).filter(substring => !taxonomyRegex.test(substring)).join(', ');
    }

    else {
        if (input.value == "" && value.includes(",")) {
            input.value += `"${value} (${issueID})"`;
        }
        else if (input.value == "") {
            input.value += `${value} (${issueID})`;
        }
        else {
            if (value.includes(",")) {
                input.value += `, "${value} (${issueID})"`;
            }
            else {
                input.value += `, ${value} (${issueID})`;
            };
        };
        let newLi = document.createElement("li")
        newLi.classList.add("selected");
        newLi.appendChild(document.querySelector(`[issue-id="${issueID}"]`).cloneNode(true));
        selectedIssuesList.appendChild(newLi);
        newLi.firstChild.addEventListener('click',  (event) => {syncSameIssues(event.target)});
    }
    sameIssues = document.querySelectorAll(`[issue-id="${issueID}"]`);
    for (let j=0; j<sameIssues.length; j++) {
        sameIssues[j].classList.toggle("selected");
        if (selectedIssuesList.contains(sameIssues[j]) && !sameIssues[j].classList.contains("selected")) {
            sameIssues[j].closest("li").remove();
        }
    }
};


function selectIssues() {
    let selectedIssues = document.querySelectorAll(".issue-name.selected");
    let selectedIssuesList = document.querySelector("#selected-issues-list");
    let splitRegex = new RegExp(`(?<=[\)\"]), `);
    let input = document.getElementById("edit-field-topics-target-id");
    let inputValueArray = input.value.split(splitRegex);
    let issues = document.querySelectorAll(".issue-name");

    // sync selected issues with values in input
    for(let i=0; i<selectedIssues.length; i++) {
        if (input.value.indexOf(selectedIssues[i].innerHTML) === -1) {
            selectedIssues[i].classList.toggle("selected");
            if ((selectedIssuesList.contains(selectedIssues[i]))) {
                selectedIssues[i].closest("li").remove();
            }
        };
    };
    
    for(let i=0; i<issues.length; i++) {
        // if issue is in input value array and not selected, select it
        for(let index=0; index<inputValueArray.length; index++) {
            if (inputValueArray[index].indexOf(issues[i].innerHTML) !== -1 ) {
                if (!issues[i].classList.contains("selected")) {
                    issues[i].classList.toggle("selected");
                    selectedIssuesList = document.querySelector("#selected-issues-list");
                    if (selectedIssuesList.innerHTML.indexOf(issues[i].outerHTML) === -1) {
                        let newLi = document.createElement("li")
                        newLi.classList.add("selected");
                        newLi.appendChild(document.querySelector(`[issue-id="${issues[i].getAttribute('issue-id')}"]`).cloneNode(true));
                        selectedIssuesList.appendChild(newLi);
                        newLi.firstChild.addEventListener('click',  (event) => {syncSameIssues(event.target)});
                    };
                };
            };
        };
        if (!selectedIssuesList.contains(issues[i])) {
            issues[i].addEventListener('click', (event) => syncSameIssues(event.target));
        }
    };
};