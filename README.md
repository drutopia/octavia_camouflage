# README

A free-for-all-to-contribute-to set of [skins](https://drupal.org/project/skins) for Drutopia's [Octavia theme](https://drupal.org/project/octavia).

Quality, generic ones should move directly into Octavia, but everything can go here.

## Octavia

Octavia is a subtheme of base theme Bulma for Drupal. 
It has Bulma CSS framework, Sass, and Font Awesome built in.

## Getting Started

### Adding a new skin

```
mkdir -p skins/newskin
```

This directory just holds a screenshot!  Edit `octavia_camouflage.skins.yml` to add it:

```
newskin:
  name: 'New Skin'
  description: 'A new skin for New Site.'
  screenshot: 'skins/newskin/screenshot.png'
```

Here's where the action starts, add a SCSS (Sass CSS) file with the same name as your new skin and add `import 'bulma;'` to the top of the file.  The cool thing about skins and compiling CSS from SCSS is you aren't just adding a few styles on top, you are getting to load and manipulate (or leave out entirely) the Bulma framework that the underlying Octavia theme is built to work with.

If you want to use an existing skin's .scss file, run:

```
cp src/global/solidaritynetwork.scss src/global/newskin.scss
```

Make sure to add a line to the `css` file for compiling your css:

```
sassc src/global/newskin.scss dist/css/newskin.css
```

Then if you need custom templates, they go here:

```
mkdir -p src/skins/newskin
```

### Browser Support

Autoprefixer & Babel is set to support:

* IE >= 9
* Last 3 versions of modern browsers.

These can be updated at any time within the `package.json`.

### Run the following commands from the theme directory

If you haven't yet, [install nvm](https://github.com/creationix/nvm).

#### Use the right version of node with:

`nvm use`

_This command will look at your `.nvmrc` file and use the version node.js specified in it. This ensures all developers use the same version of node for consistency._

#### If that version of node isn't installed, install it with:

`nvm install`

#### Install npm dependencies with

`npm install`

_This command looks at `package.json` and installs all the npm dependencies specified in it.  Some of the dependencies include gulp, autoprefixer, gulp-sass and others._

#### Compile CSS from Sass

`./css`

Or to watch and automatically compile:

`./css-watch`

##### Prerequisites


```bash
sudo apt install inotify-tools sassc
```

<!-- writeme -->
Octavia
=======

A base theme for the Drutopia distribution based on Bulma.

 * https://gitlab.com/drutopia/octavia
 * Issues: https://gitlab.com/drutopia/octavia/issues
 * Source code: https://gitlab.com/drutopia/octavia/tree/8.x-1.x
 * Keywords: theme, flexbox, bulma, templates, styles, drutopia
 * Package name: drupal/octavia


### Requirements

 * drupal/bulma ^1.0-alpha2


### License

GPL-2.0+

<!-- endwriteme -->
